# CNN for prediction of the architectural style 

The purpose of the work is to predict the architectural style shown in the picture using a classificator trained to detect different styles (according to dataset and model). 
In this study we will look for different architectures to build CNN for image classification on different datasets with different numbers of classes.

## Sourse
in case of problems with files in repo - you can see results and play with code [here](https://colab.research.google.com/drive/16cB2zfuA6GG1pfkK6bdzsWd401uu6k_d#scrollTo=J3U3Kq9OmyQ3)
For data set you can find one on kaggle or ask for it directly
